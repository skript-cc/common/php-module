# A ‘module’ loader for PHP?

This repository is a collection of research into the (non)sense of building a
loader for something that are called ‘modules’ in other programming languages.
i.e. namespaced code other than classes.

## Problem: paradigm inequality in PHP development

The use operator in PHP let developers *alias/import* namespaces. In this
context *importing* means: making a namespace available in the local (file)
scope. It doesn’t mean that any code for that namespace is included when it
resides in a file. Either one of the `include`/`require` statements or 
autoloading have to be used for that.

With the help of (PSR) autoloading the popular (and standardized) practice
amongst PHP developers became to rely on autoloading for file inclusion. This
almost makes it appear if PHP’s namespace system includes code files. There’s
only one important caveat: this illusion breaks down when you stop working with
classes. Anything other than classes are not, and *can not*, be autoloaded.

There are workarounds of course. It’s possible to fall back to manual
`require_once` statements. Or to preload all non-class files, for example with
composers `autoload.files` option. It’s even suggested to shoehorn all code
back in an OOP paradigm by converting plain functions to (static) class methods.

But why do we treat code that’s not Object Oriented differently in the first
place? Why can’t we import functions with the same ease as classes? What can we
realistically do about this inequality and bias in PHP development?

## Suggestion: find a better way for file inclusion

I would suggest to reconsider the current practice of file imports in PHP and
look at other programming languages for inspiration (such as es6 and python).
Which approaches make sense in PHP programming and which do not?

To go short, this are some preliminary findings:

* The `import x from y` syntax is a readable and familiar expression, and easy
  to convert to a functional equivalent: `import('x', from('y'))`
* It doesn’t make much sense to use it in the same way as is done in es6 and
  python. In those languages `x` refers to a symbol and `y` to the namespace. In
  PHP all symbols in namespaces become available when including a file.
* It makes more sense to use a namespace for `x` and a PSR-4 map for `y`. The
  map maps namespaces to base directories in the same way as is done when
  autloading classes. This makes it possible to import files by their namespace
  while they can be placed in different base directories.
* Architecture of an import function like this can be very clean and extendable.
  Apart from including plain php files, it could be made to include other file
  types (such as json) too. For example: 
  `$json = import(json('path/to/json_file'), from(CONFIG));`.

Open questions:

* Does this approach have a noticeable impact on performance?
* What’s the best way of using this? Are import calls to be placed at the
  top of each file? Is it best to use this next to autoloading classes, or can
  it also be used as a replacement?
* What would be the recommended practice for using imports like this in packages
  which are distributed through composer repositories?
* Should I be concerned about the fact that calling an import function in a file
  causes a side-effect and goes against the PSR-1 standard as specified in 
  [section 2.3](https://www.php-fig.org/psr/psr-1/#23-side-effects)?

## Example

See [src/Module.php](/src/Module.php) and [tests/index.php](/tests/index.php)
for a working prototype.

```php
<?php

use function Skript\Module\{import, from};

const VENDORS = [
    'VendorA\\' => 'vendor/vendor-a/src/',
    'VendorB\\' => 'vendor/vendor-b/'
];

// includes vendor/vendor-a/src/Module1.php
import('VendorA\Module1', from(VENDORS));

// includes vendor/vendor-a/src/Module2.php and vendor/vendor-b/Module3.php
import(['VendorA\Module2', 'VendorB\Module3'], from(VENDORS));
```

## Motivation

In the world of PHP development OOP is king. According to [some](https://alanstorm.com/python_imports_for_php_developers/),
PHP developers have gotten used to encapsulate their code in classes to prevent
the global scope from getting polluted. This, in turn, caused by PHP’s initial
lack of a namespace system.

Since PHP 5.3 [namespaces](https://www.php.net/manual/en/language.namespaces.rationale.php)
are also available in PHP. However, PHP developers have gotten used to things
like autoloading classes and OOP paradigms. Since [autoloading for functions](https://stackoverflow.com/questions/4737199/autoloader-for-functions)
is not possible, it’s tempting to write functions as (static) class methods
after all and stick to the OOP paradigm.

Since I’m charmed of the elegancy of functional programming, this resulted in a
quest to find a simple mechanism for loading ‘PHP modules’.