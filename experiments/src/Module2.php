<?php

namespace Skript\Module;

const DEFAULT_FILE_EXT = '.php';

function import($loader, $finder = null)
{
    // shorthands
    if (!(is_callable($loader) && is_callable($finder))) {
        // import('Some/Path');
        if (is_string($loader) && $finder === null) {
            $loader = path($loader);
        }
        // import('*', from('Some/Module'))
        elseif ($loader === '*') {
            $loader = module();
        }
        // import('sayHelloTo', from('Hello'))
        elseif (is_string($loader) && is_callable($finder)) {
            $loader = fn($loader);
        }
        // import(['world', 'moon', 'mars'], from('Space/Hello'))
        elseif (is_array($loader)) {
            $loader = fn(...$loader);
        }
    }
    
    [$ns, $foundPaths] = $finder
        ? call_user_func($finder)
        : [null, basepath()];
    
    return call_user_func_array($loader, [$ns, $foundPaths]);
}

function path(...$paths): \closure
{
    // path loader
    return function (string $ns = null, array $basepaths = []) use ($paths) {
        $mappedPaths = array_reduce(
            $paths,
            function ($paths, $path) use ($basepaths) {
                $fileExt = pathinfo($path, PATHINFO_EXTENSION) 
                    ? ''
                    : DEFAULT_FILE_EXT;
                return array_merge(
                    $paths,
                    array_map(
                        mappath($path, $fileExt),
                        !count($basepaths) ? [''] : $basepaths
                    )
                );
            },
            []
        );
        $results = array_map(__NAMESPACE__.'\includeOnce', $mappedPaths);
        return count($results) > 1 ? $results : array_shift($results);
    };
}

function fn(...$fns)
{
    // function loader
    return function (string $ns, array $paths) use ($fns) {
        path(...$paths)($ns, ['']);
        $results = array_map(
            __NAMESPACE__.'\createFn',
            array_map(
                function ($fn) use ($ns) { return implode('\\', [$ns, $fn]); },
                $fns
            )
        );
        return count($results) > 1 ? $results : array_shift($results);
    };
}

function createFn($fn): \closure
{
    return function (...$args) use ($fn) {
        return call_user_func_array($fn, $args);
    };
}

function module(...$fns)
{
    // module loader
    return function (string $ns, array $paths) use ($fns): Module {
        path(...$paths)($ns, ['']);
        return new Module($ns, $fns);
    };
}

function from(string $path)
{
    // finder
    return function () use ($path) {
        $basepaths = basepath();
        $namespace = str_replace('/', '\\', $path);
        return [
            $namespace,
            array_map(mappath($path), !count($basepaths) ? [''] : $basepaths)
        ];
    };
}

function mappath(string $path, string $suffix='') {
    return function($basepath) use ($path, $suffix) {
        return implode(DIRECTORY_SEPARATOR, [$basepath, $path]).$suffix;
    };
}

function basepath(string $path = null): array
{
    static $_path = [];
    if ($path === '') {
        $_path = []; // passing an empty string clears all previous paths
    } elseif ($path !== null) {
        $_path[] = $path;
    }
    return $_path;
}

function includeOnce(string $path) {
    return include_once $path;
}

class Module
{
    protected $namespace;
    protected $whitelist;
    
    public function __construct(string $namespace, array $whitelist=null)
    {
        $this->namespace = $namespace;
        $this->whitelist = $whitelist;
    }
    
    public function __call(string $name, $args)
    {
        if (!in_array($name, $this->whitelist)) {
            throw new \BadMethodCallException();
        }
        return call_user_func_array($this->namespace.'\\'.$name, $args);
    }
}