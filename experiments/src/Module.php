<?php

namespace Skript\Module;

const DEFAULT_FILE_EXT = '.php';
const DEFAULT_FILE_MAP = ['' => '.'];

function import(
    $namespace,
    array $from = array(
        'type' => 'map',
        'fileMap' => DEFAULT_FILE_MAP,
        'fileExt' => DEFAULT_FILE_EXT
    )
): ?Module {
    $includePath = '';
    extract($from);
    switch ($type) {
        case 'map':
            $includePaths = getPathsFromFileMap($namespace, $fileMap, $fileExt);
            // fail hard when multiple paths are found
            if (count($includePaths) > 1) {
                throw new AmbiguousImportException($namespace, $includePaths);
            }
            $includePath = array_shift($includePaths);
            break;
        case '_path':
            $includePath = $path
                . DIRECTORY_SEPARATOR
                . str_replace('\\', '/', $namespace)
                . $fileExt;
            break;
        case 'path':
            $includePath = $path
                . DIRECTORY_SEPARATOR
                . str_replace('\\', '/', $namespace)
                . $fileExt;
            break;
        default:
            // intentionally left blank
    }
    return (include_once $includePath) ? new Module($namespace) : null;
}

function importAll(array $namespaces, array $from = null)
{
    return array_map(
        function ($ns) use ($from) { return import($ns, $from); },
        $namespaces
    );
}

function from(string $path): array
{
    return [
        'type' => 'path',
        'path' => $path,
        'fileExt' => DEFAULT_FILE_EXT
    ];
}

function fromMap(array $map): array
{
    return [
        'type' => 'map',
        'fileMap' => $map,
        'fileExt' => DEFAULT_FILE_EXT
    ];
}

function fromRegistry(string $name): array
{
    return fromMap(registry($name));
}

function getPathsFromFileMap(
    string $namespace,
    array $fileMap,
    string $fileExt
): array {
    $namespace = trim($namespace, '\\');
    $lookupPaths = array_map(
        function ($ns, $path) use ($namespace, $fileExt) {
            $nsParts = array_filter(explode('\\', $ns));
            $path = rtrim($path, DIRECTORY_SEPARATOR);
            $diff = array_diff(explode('\\', $namespace), $nsParts);
            return implode(
                DIRECTORY_SEPARATOR,
                array_merge([$path], $nsParts, $diff)
            ) . $fileExt;
        },
        array_keys($fileMap),
        array_values($fileMap)
    );
    return array_filter($lookupPaths, 'file_exists');
}

function registry(string $name, array $map=[]): array
{
    static $registry = [];
    $registry = [$name => $map + ($registry[$name] ?? [])] + $registry;
    return $registry[$name];
}

class Module
{
    protected $namespace;
    
    public function __construct(string $namespace)
    {
        $this->namespace = $namespace;
    }
    
    public function __call(string $name, $args)
    {
        return call_user_func_array($this->namespace.'\\'.$name, $args);
    }
}

class AmbiguousImportException extends \ErrorException
{
    protected $namespace;
    protected $includePaths;
    
    public function __construct(
        string $namespace,
        array $includePaths,
        int $code = 0,
        Exception $previous = null
    ) {
        $message = "Ambiguous import, found two modules '{$namespace}' ('"
            . implode("', '", $includePaths)
            . "')";
        parent::__construct($message, $code, $previous);
    }
}