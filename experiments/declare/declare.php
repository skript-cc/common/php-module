<?php

declare(import='VendorA\Module1');

echo "This declare directive is not recognized, will it be ignored, or does it throw a warning?";

/*
Output:
PHP Warning:  Unsupported declare 'import' in /home/kevin/Workspace/lib/module/tests/declare.php on line 3
This declare directive is not recognized, will it be ignored, or does it throw a warning?

Futhermore, this is an E_COMPILE_WARNING (https://www.php.net/manual/en/errorfunc.constants.php),
which cannot be handled by a user defined error handler.

But it can be surpressed with error_reporting(E_ALL & ~E_COMPILE_WARNING);
*/