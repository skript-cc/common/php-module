<?php

// does not handle E_COMPILE_WARNING
set_error_handler(function(...$errorInfo) {
    var_dump($errorInfo);
});

// does surpress E_COMPILE_WARNING
error_reporting(E_ALL & ~E_COMPILE_WARNING);

require 'declare.php';