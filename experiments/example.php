<?php

require_once __DIR__.'/src/Module.php';

use function \Skript\Module\{
    import,
    importAll,
    from,
    fromMap,
    fromRegistry,
    registry
};
use function Space\Hello\venus as helloVenus;

function plainImport()
{
    import('Space\Hello');
}

function exampleImportFrom()
{
    import('Hello', from('example/'));
    import('Space\Hello', from('example/'));
    
    Hello\sayHelloTo('import from');
    Space\Hello\world();
    Space\Hello\moon();
    Space\Hello\mars();
    
    helloVenus();
}

function exampleImportAll()
{
    importAll(['Hello', 'Space\Hello'], from('example/'));

    Hello\sayHelloTo('importAll');
    Space\Hello\world();
    Space\Hello\moon();
    Space\Hello\mars();
    Space\Hello\venus();
}

function exampleRegistry()
{
    registry('myRegistry', [
        'Hello' => 'example',
        'Space\Hello' => 'example'
    ]);
    import('Space\Hello', fromRegistry('myRegistry'));
    
    Space\Hello\world();
    Space\Hello\moon();
    Space\Hello\mars();
    Space\Hello\venus();
}

function exampleRegistry2()
{
    registry('myRegistry', ['' => 'example']);
    importAll(['Hello', 'Space\Hello'], fromRegistry('myRegistry'));
    
    Hello\sayHelloTo('registry');
    Space\Hello\world();
    Space\Hello\moon();
    Space\Hello\mars();
    Space\Hello\venus();
}

function exampleReturn()
{
    $helloSpace = import('Space\Hello', from('example'));
    $helloSpace->world();
}

function exampleReturn2()
{
    [$hello, $helloSpace] = importAll(['Hello', 'Space\Hello'], from('example'));
    $hello->sayHelloTo('return module');
    $helloSpace->world();
}

function exampleAmbiguousImport()
{
    import('Hello', fromMap([
        '' => 'example',            // -> example/Hello.php
        'Hello' => 'example/Space'  // -> example/Space/Hello.php
    ]));
    
    // throws AmbiguousImportException
}

exampleImportFrom();
// exampleImportAll();
// exampleRegistry();
// exampleRegistry2();
// exampleReturn();
// exampleReturn2();
// exampleAmbiguousImport();
