<?php

namespace Space\Hello;

use function \Skript\Module\{import, from};
use function \Hello\sayHelloTo;

global $testModule2;

if ($testModule2) {
    import(__DIR__.'/../Hello'); // this is no different than include once...
} else {
    import('Hello', from(__DIR__.'/..'));
}

function world() {
    sayHelloTo('world');
}

function moon() {
    sayHelloTo('moon');
}

function mars() {
    sayHelloTo('mars');
}

function venus() {
    sayHelloTo('venus');
}