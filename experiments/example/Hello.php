<?php

namespace Hello;

function sayHelloTo(string $subject) {
    echo "Hello {$subject}\n";
}