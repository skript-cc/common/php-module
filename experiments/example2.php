<?php

require_once __DIR__.'/src/Module2.php';

use function \Skript\Module\{import, from, path, basepath, module, fn};

$testModule2 = true;

function exampleImportPath()
{
    basepath(__DIR__.'/example');
    
    import(path('Space/Hello')); // includes example/Space/Hello.php
    
    // shorthand: import('Space/Hello')
    
    Space\Hello\world();
    Space\Hello\moon();
    Space\Hello\venus();
}

function exampleImportAbsolutePath()
{
    import(path('Space/Hello'), from(__DIR__.'/example'));
    
    Space\Hello\world();
    Space\Hello\moon();
    Space\Hello\venus();
}

function exampleImportAbsolutePath2()
{
    import(path(__DIR__.'/example/Space/Hello'));
    
    Space\Hello\world();
    Space\Hello\moon();
    Space\Hello\venus();
}

function exampleImportModuleWithAllFunctions()
{
    basepath(__DIR__.'/example');
    
    // includes example/Space/Hello.php and returns an instance of Module
    $all = import(module(), from('Space/Hello'));
    $all->world();
    $all->moon();
    $all->mars();
    $all->venus();
    
    // shorthand: import('*', from('Space/Hello'))
}

function exampleImportModuleWithSomeFunctions()
{
    basepath(__DIR__.'/example');
    
    // includes example/Space/Hello.php and returns an instance of Module
    $all = import(module('world', 'moon', 'mars'), from('Space/Hello'));
    $all->world();
    $all->moon();
    $all->mars();
    $all->venus(); // throws error
}

function exampleImportFunction()
{
    basepath(__DIR__.'/example');
    
    // includes example/Hello.php and returns a closure or callable
    $sayHelloTo = import(fn('sayHelloTo'), from('Hello'));
    $sayHelloTo('imported function');
    
    // shorthand: import('sayHelloTo', from('Hello'))
}

function exampleImportFunctions()
{
    basepath(__DIR__.'/example');
    
    // includes example/Hello.php and returns a closure or callable
    [$world, $moon, $mars] = import(fn('world', 'moon', 'mars'), from('Space/Hello'));
    $world();
    $moon();
    $mars();
    
    // shorthand: import(['world', 'moon', 'mars'], from('Space/Hello'))
}

// exampleImportAbsolutePath();
exampleImportAbsolutePath2();