<?php

require_once __DIR__.'/../src/Module.php';

use function Skript\Module\{import, from};

/**
 * This is a psr4 map similar to the format used by composer
 */
const PSR4_MAP = [
    'VendorA\\' => 'vendor/vendor-a/src/',
    'VendorB\\' => 'vendor/vendor-b/'
];

/**
 * Scenario: import a single namespace
 *
 * Given a namespace 'VendorA\Module1' defined in a file vendor/vendor-a/src/Module1.php
 * When import is called
 *   and the first argument is set to the namespace 'VendorA\Module1'
 *   and the second argument is a PSR-4 mapper that maps the VendorA namespace name to the path vendor/vendor-a/src/
 * Then the file vendor/vendor-a/src/Module1.php should be included
 *   and all functions defined in that namespace should be available
 */
function importSingleNamespace()
{
    $result = import('VendorA\Module1', from(PSR4_MAP));
    
    $defFn = get_defined_functions()['user'];
    var_dump([
        'File is included' => $result === 1,
        'All namespace functions are available' => 
            array_search('vendora\module1\test1', $defFn) !== false
            && array_search('vendora\module1\test2', $defFn) !== false,
        //'defined_user_fn' => $defFn
    ]);
}

/**
 * Scenario: import multiple namespaces
 */
function importMultipleNamespaces()
{
    $result = import(['VendorA\Module1', 'VendorB\Module1'], from(PSR4_MAP));
    
    $defFn = get_defined_functions()['user'];
    var_dump([
        'Return value is array' => is_array($result),
        'Files are included' => $result[0] === 1 && $result[1] === 1,
        'All namespace functions are available' => 
            array_search('vendora\module1\test1', $defFn) !== false
            && array_search('vendora\module1\test2', $defFn) !== false
            && array_search('vendorb\module1\test1', $defFn) !== false
            && array_search('vendorb\module1\test2', $defFn) !== false
        //'defined_user_fn' => $defFn
    ]);
}

/**
 * Scenario: use from function standalone
 */
function useFromStandalone()
{
    $result = require_once from(PSR4_MAP)('VendorA\Module1');
    
    $defFn = get_defined_functions()['user'];
    var_dump([
        'File is included' => $result === 1,
        'All namespace functions are available' => 
            array_search('vendora\module1\test1', $defFn) !== false
            && array_search('vendora\module1\test2', $defFn) !== false,
        //'defined_user_fn' => $defFn
    ]);
}

/**
 * Scenario: memoize calls to from
 */
function memoizeFrom() 
{
    $count = 0;
    $resolver = function ($options, $map, $namespace) use (&$count) {
        $count++;
        return '/test/path';
    };
    
    $path1 = from(PSR4_MAP, compact(['resolver']))('VendorA\Module1');
    $path2 = from(PSR4_MAP, compact(['resolver']))('VendorA\Module1');
    $path3 = from([])('VendorA\Module1');
    
    var_dump([
        'Returns memoized result' => $count === 1,
        'Returns new result with different map' => $path2 !== $path3
    ]);
}

/**
 * Scenario: import from a fallback directory
 */
function importFromFallback()
{
    $result = import('VendorA\Module1', from(['' => 'vendor/fallback/']));
    
    $defFn = get_defined_functions()['user'];
    var_dump([
        'File is included' => $result === 1,
        'All namespace functions are available' => 
            array_search('vendora\module1\test1', $defFn) !== false
            && array_search('vendora\module1\test2', $defFn) !== false
            && array_search('vendora\module1\fallback', $defFn) !== false,
        //'defined_user_fn' => $defFn
    ]);
}

/**
 * Scenario: map a namespace to multiple base directories
 */
function importNamespaceWithLookupInMultipleDirectories()
{
    $map = [
        'VendorB' => [
            'vendor/vendor-b/',
            'vendor/fallback/VendorB' // file does not exist, but does match
        ],
    ];
    // to import the file correctly in this scenario, we have to filter paths
    // that do not exists. This might have a negative impact on performance.
    $mapOptions = ['filter_non_existing_paths' => true];
    
    $result = import('VendorB\Module1', from($map, $mapOptions));
    
    $defFn = get_defined_functions()['user'];
    var_dump([
        'File is included' => $result === 1,
        'All namespace functions are available' => 
            array_search('vendorb\module1\test1', $defFn) !== false
            && array_search('vendorb\module1\test2', $defFn) !== false,
        //'defined_user_fn' => $defFn
    ]);
}

/**
 * Scenario: the imported module is mapped to two paths.
 *
 * Expect the last defined (matching) map directive to be used
 */
function importNamespaceWithMultipleMatches()
{
    $map = [
        '' => 'vendor/fallback/',
        'VendorA' => 'vendor/vendor-a/src/',
    ];
    $result = import('VendorA\Module1', from($map));
    
    $defFn = get_defined_functions()['user'];
    var_dump([
        'File is included' => $result === 1,
        'All namespace functions are available' => 
            array_search('vendora\module1\test1', $defFn) !== false
            && array_search('vendora\module1\test2', $defFn) !== false,
        'The fallback function is not available' => 
            array_search('vendora\module1\fallback', $defFn) === false
        //'defined_user_fn' => $defFn
    ]);
}

/**
 * Scenario: implicit from. I don't plan to support this. Instead, wrap the
 * import function in another import function and define a default from.
 */
function importWithImplicitFrom() {
    import('VendorA\Module1');
}

/**
 * Scenario: custom loaders
 */
function importWithCustomLoader() {
    import(module('VendorA\Module1'), from(PSR4_MAP));
}

call_user_func($argv[1]);