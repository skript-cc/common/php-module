<?php

namespace VendorC\TestModuleImports\Module1;

// Problem 1: import is not defined (Skript\Module\import would also be undefined, because functions are not autoloaded)
// Problem 2: this does not conform to PSR-1
import('VendorA\Module');

use function Module1\test1;

function testDependency() {
    // call function from a package distributed by another vendor
    test1();
}