<?php

namespace Skript\Module;

const FROM_DEFAULT_OPTIONS = [
    'filter_non_existing_paths' => false,
    'use_cache' => true,
    'resolver' => __NAMESPACE__.'\getPathFromMap'
];

/**
 * Resolves a namespace to a path, by using a map with [namespace => basedir]
 * key/value pairs.
 *
 * Examples:
 *
 * namespaceToPaths('VendorA\Module1', ['VendorA' => 'vendor/vendor-a/src/']);
 * // returns ['vendor/vendor-a/src/Module1.php']
 *
 * namespaceToPaths('VendorA\Module1', ['' => 'vendor/fallback/']);
 * // returns ['vendor/fallback/VendorA/Module1.php']
 *
 * namespaceToPaths('VendorA\Module1', [
 *   '' => 'vendor/fallback/',
 *   'VendorA' => 'vendor/vendor-a/src/'
 * ]);
 * // returns ['vendor/fallback/VendorA/Module1.php', 'vendor/vendor-a/src/Module1.php']
 *
 * @param string The namespace to lookup
 * @param array A map with [namespace => basedir(s)] key/value pairs.
 *   - When namespace is an empty string any namespace will be matched and 
 *     resolved against the corresponding base dir(s).
 *   - When basedir is an array, matching namespaces will be resolved against
 *     both paths.
 * @param string File extension (or path suffix) to append
 */
function namespaceToPaths(
    string $namespace,
    array $map,
    string $fileExt='.php'
): array {
    $paths = array_map(
        function ($ns, $paths) use ($namespace, $fileExt) {
            // skip namespace keys that do not match with the namespace
            if (strlen($ns) > 0 && strpos($namespace, $ns) !== 0) {
                return null;
            }
            $nsParts = array_filter(explode('\\', $ns));
            $diff = array_diff(explode('\\', $namespace), $nsParts);
            return array_map(
                function ($path) use ($namespace, $nsParts, $diff, $fileExt) {
                    $path = rtrim($path, DIRECTORY_SEPARATOR);
                    return implode(
                        DIRECTORY_SEPARATOR,
                        array_merge([$path], $diff)
                    ) . $fileExt;
                },
                !is_array($paths) ? [$paths] : $paths
            );
        },
        array_keys($map),
        array_values($map)
    );
    
    // filter out non matches and flatten paths
    return array_reduce(array_filter($paths), 'array_merge', []);
}

function getPathFromMap(array $options, array $map, string $namespace): ?string
{
    $paths = namespaceToPaths($namespace, $map);
    
    // Filter out the non existent paths? This might be slow, but can also
    // be helpful in avoiding conflicts.
    if ($options['filter_non_existing_paths']) {
        $paths = array_filter($paths, 'file_exists');
    }
    
    // when there are multiple paths (left), the last one wins
    return end($paths) ?: null;
}

/**
 * Create a namespace to path mapper
 */
function from(
    array $map,
    array $options = []
): \closure {
    $options = $options + FROM_DEFAULT_OPTIONS;
    $getPath = function (string $namespace) use ($map, $options): ?string {
        return call_user_func_array(
            $options['resolver'],
            [$options, $map, $namespace]
        );
    };
    return $options['use_cache'] ? _memoFrom($getPath, $map) : $getPath;
}

function _memoFrom(callable $fn, &$map): \closure
{
    static $cache = [];
    return function(string $namespace) use ($fn, &$cache, &$map) {
        if (!isset($cache[$namespace]) || ($cache[$namespace]['map'] !== $map)) {
            $cache[$namespace] = [
                'map' => $map,
                'result' => $fn($namespace)
            ];
        }
        return $cache[$namespace]['result'];
    };
}

/**
 * Create a namespace loader
 */
function ns($namespace)
{
    return [
        'values' => !is_array($namespace) ? [$namespace] : $namespace,
        'fn' => function(string $path=null, string $ns=null) {
            return $path ? include_once($path) : false;
        }
    ];
}

/**
 * Call import loader with values transformed by mapper function
 */
function import($loader, callable $mapFn)
{
    if (!is_array($loader) || !(isset($loader['fn']) && isset($loader['values']))) {
        $loader = ns($loader);
    }
    $results = array_map(
        $loader['fn'],
        array_map($mapFn, $loader['values']),
        $loader['values']
    );
    return count($loader['values']) === 1 ? end($results) : $results;
}